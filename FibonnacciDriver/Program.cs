﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fibonacci;


namespace FibonacciDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 9;

            Console.WriteLine("Recursive: " + Fibonacci.Fibonacci.FibonacciRecursive(n));
            Console.WriteLine("Dynamic: " + Fibonacci.Fibonacci.FibonacciDynamic(n));
            Console.WriteLine("Dynamic Optimzed For Space: " + Fibonacci.Fibonacci.FibonacciDynamicOptimizedForSpace(n));
            Console.WriteLine("Matrix Transform: " + Fibonacci.Fibonacci.FibonacciMatrixTransform(n));
            Console.WriteLine("Matrix Transform Optimzed for speed: " + Fibonacci.Fibonacci.FibonaccimatrixTransformOptimizedForSpeed(n));
            Console.ReadLine();
        }
    }
}
