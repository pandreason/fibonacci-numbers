﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fibonacci
{
    public class Fibonacci
    {
        public static int FibonacciRecursive(int n)
        {
            if (n < 0)
                throw new ArgumentOutOfRangeException("n");

            if (n <= 1)
                return n;
            return FibonacciRecursive(n - 1) + FibonacciRecursive(n - 2);
        }

        public static int FibonacciDynamic(int n)
        {
            if (n < 0)
                throw new ArgumentOutOfRangeException("n");

            int[] fibNumbers = new int[n + 1];

            fibNumbers[0] = 0;
            fibNumbers[1] = 1;

            for (var i = 2; i <= n; i++)
                fibNumbers[i] = fibNumbers[i - 1] + fibNumbers[i - 2];

            return fibNumbers[n];
        }

        public static int FibonacciDynamicOptimizedForSpace(int n)
        {
            if (n < 0)
                throw new ArgumentOutOfRangeException("n");

            if (n <= 1)
                return n;

            int a, b, c;

            a = 0;
            b = 1;

            for (var i = 2; i <= n; i++)
            {
                c = a + b;
                a = b;
                b = c;
            }

            return b;
        }

        public static int FibonacciMatrixTransform(int n)
        {
            if (n < 0)
                throw new ArgumentOutOfRangeException("n");

            int[,] F = new int[2, 2] { { 1, 1 }, { 1, 0 } };

            if (n == 0)
                return 0;

            matrixPower(ref F, n - 1);

            return F[0, 0];
        }

        public static int FibonaccimatrixTransformOptimizedForSpeed(int n)
        {
            if (n < 0)
                throw new ArgumentOutOfRangeException("n");

            int[,] F = new int[2, 2] {{1,1},{1,0}};

            if (n == 0)
                return 0;

            matrixPowerRecursive(ref F, n - 1);

            return F[0, 0];
        }

        private static void matrixMultiply(ref int[,] F, int[,] M)
        {
            int a = F[0,0] * M[0,0] + F[0,1] * M[1,0];
            int b = F[0,0] * M[0,1] + F[0,1] * M[1,1];
            int c = F[1,0] * M[0,0] + F[1,1] * M[1,0];
            int d = F[1,0] * M[0,1] + F[1,1] * M[1,1];

            F[0,0] = a;
            F[0,1] = b;
            F[1,0] = c;
            F[1,1] = d;
        }

        private static void matrixPower(ref int[,] F, int n)
        {
            int[,] M = new int[2, 2] { { 1, 1 }, { 1, 0 } };

            for (var i= 2;i<=n;i++)
                matrixMultiply(ref F, M);
        }

        private static void matrixPowerRecursive(ref int[,] F, int n)
        {
            if (n == 0 || n == 1)
                return;

            int[,] M = new int[2, 2] { { 1, 1 }, { 1, 0 } };

            matrixPowerRecursive(ref F, n / 2);
            matrixMultiply(ref F, F);

            if (n % 2 != 0)
                matrixMultiply(ref F, M);
        }
    }
}
